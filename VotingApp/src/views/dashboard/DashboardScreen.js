import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, BackHandler, TouchableOpacity } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, CardItem, Card }  from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import MaterialCommunityIcons from '@expo/vector-icons';

class DashboardScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            //Bila Sudah Login
            //set isLogined true pada asyncstorage
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
        }
    }

    async componentDidMount() {
        await BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){
        BackHandler.exitApp()
        return true;
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    render() {
        return (
            <Container>
                <StatusBar style="dark"/>
                <Header style={{backgroundColor: '#f1f1f1'}}>
                    <Body>
                        <Title style={{fontWeight: 'bold', color: 'gray'}}>Home</Title>
                    </Body>
                </Header>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    centered: {
        alignItems: 'center'
    }
});

export default DashboardScreen