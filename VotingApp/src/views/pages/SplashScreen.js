import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, Image, Animated, ActivityIndicator } from 'react-native';
import { Button, Container, Text } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import Logo from '../../assets/images/Logo.png';

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            LogoAnime: new Animated.Value(0),
            LogoText: new Animated.Value(0),
            loadingSpinner: false
            //Bila Sudah Login
            //Lakukan Load Data seperti Profile Disini
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
        }
    }

    componentDidMount() {
        if(false){
            // await axios -> getDataPemilih, getDataKandidat (method get)
            // .then(data => {
            //     //setData AsyncStorage
            // })
            this._toDashboard();
        }else{
            //Clear AsyncStorage
            //this.props.navigation.navigate('Login')
            this._toLogin();
        }
        
    }

    _toDashboard(){
        Animated.parallel([
            Animated.spring(this.state.LogoAnime, {
                toValue: 1,
                tension: 10,
                friction: 3,
                duration: 1000,
                useNativeDriver: false // Add This line
            }).start(() => {
                this.props.navigation.navigate('Dashboard')
            }),

            Animated.timing(this.state.LogoText, {
                toValue: 1,
                duration: 1700,
                useNativeDriver: true // Add This line
            }),
        ]).start();
    }

    _toLogin(){
        Animated.parallel([
            Animated.spring(this.state.LogoAnime, {
                toValue: 1,
                tension: 10,
                friction: 3,
                duration: 1000,
                useNativeDriver: false // Add This line
            }).start(() => {
                this.props.navigation.navigate('Login')
            }),

            Animated.timing(this.state.LogoText, {
                toValue: 1,
                duration: 1700,
                useNativeDriver: true // Add This line
            }),
        ]).start();
    }

    componentWillUnmount() {
        //Clear Routes Splash
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar style="light"/>
                <Animated.View style={{
                    opacity: this.state.LogoAnime,
                    top: this.state.LogoAnime.interpolate({
                        inputRange: [0, 1],
                        outputRange: [80, 0]
                    })
                }}>
                    <Image source={Logo}/>
                </Animated.View>

                <Animated.View style={{
                    opacity: this.state.LogoText
                }}>
                    <Text style={styles.logoText}>Voting App</Text>
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#677C9B',
        justifyContent: 'center',
        alignItems: 'center'
    },

    logoText: {
        color: '#FFFFFF',
        fontFamily: 'ProductSans-Bold',
        fontSize: 30,
        marginTop: 29.1,
        fontWeight: '300'
    }
})

export default SplashScreen