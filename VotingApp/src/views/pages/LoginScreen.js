import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, BackHandler } from 'react-native';
import { Button, Card, CardItem, Body, Container, Content } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';

class LoginScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            //Bila Sudah Login
            //set isLogined true pada asyncstorage
            //Saat sudah terload baru pindah ke Dashboard
            //Data di inputkan ke AsyncStorage
            //Agar aplikasi berjalan smooth
        }
    }

    async componentDidMount() {
        await BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){
        BackHandler.exitApp()
        return true;
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    render() {
        return (
            <Container style={{padding: 20, backgroundColor: '#6a51ae'}}>
            <StatusBar style="light"/>
                <Grid>
                    <Row size={1}></Row>
                    <Col size={1}>
                        <Card style={styles.container}>
                            <CardItem>
                                <Grid>
                                    <Col size={1} style={styles.centered}>
                                        <Button success block onPress={() => this.props.navigation.navigate('Dashboard')}>
                                            <Text>Next</Text>
                                        </Button>
                                    </Col>
                                </Grid>
                            </CardItem>
                        </Card>
                    </Col>
                    <Row size={1}></Row>
                </Grid>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    centered: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default LoginScreen