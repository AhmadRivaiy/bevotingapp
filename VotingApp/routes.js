import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from './src/views/pages/SplashScreen'
import LoginScreen from './src/views/pages/LoginScreen'
import DashboardScreen from './src/views/dashboard/DashboardScreen';

const VotingApp = createStackNavigator({
    Splash: { screen: SplashScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Login: { screen : LoginScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Dashboard: { screen : DashboardScreen,
      navigationOptions: {
        headerShown: false
      }
    }
  },
  {
    initialRouteName: 'Splash'
  })
  
  export default createAppContainer(VotingApp);