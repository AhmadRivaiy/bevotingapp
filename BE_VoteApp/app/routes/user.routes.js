const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/welcome_screen",
    [authJwt.verifyTokenApp],
    controller.welcomeAll
  );

  app.get("/api/getsync/datauser",
    [authJwt.verifyTokenApp],
    controller.getSyncUser
  );

  app.post("/api/voteapp/vote", 
    [authJwt.verifyTokenApp],
    controller.voteCandidate
  );

  app.get("/api/get/quickcount",
    [authJwt.verifyTokenApp],
    controller.getQuickCount
  );
  // app.get(
  //   "/api/test/kasi",
  //   [authJwt.verifyToken, authJwt.isModerator],
  //   controller.kasiBoard
  // );

  // app.get(
  //   "/api/test/pimpinan",
  //   [authJwt.verifyToken, authJwt.isAdmin],
  //   controller.pimpinanBoard
  // );

  // app.get(
  //   "/api/test/petugas_agenda",
  //   [authJwt.verifyToken, authJwt.isAdmin],
  //   controller.agendaBoard
  // );
};