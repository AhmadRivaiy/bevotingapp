
const config = require("../config/auth.config");
const UserModel = require("../models/user.model");

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signin = (req, res, next) => {
  new UserModel().getUser(req.body.no_tlp)
    .then(user => {
      // if (!user) {
      //   return res.status(404).send({ message: "User Not found." });
      // }

      var token = jwt.sign({ appName: 'EVoting', userId: user.id }, config.secretApp);

      res.status(200).send({
        data : {
          status : '1',
          message : 'Login Sukses!',
          is_voted: user.is_voted,
          is_logined: user.is_logined,
          define_device: user.define_device,
          device_id: user.unique_device_id,
          candidate_vote: user.candidate_vote,
          payload : {
            id: user.id,
            nama: user.nama,
            email: user.email,
            nisn: user.nisn,
            npsn: user.npsn_sekolah_sekarang,
            sekolah: user.sekolah,
            phone_number: user.no_tlp
          }
        },
        accessToken: {
          tokenIs: token
        }
      });
    }).catch(err => {
      var details = {
        parent: err.parent,
        nameErr: err.name,
        messageErr: err.message
      }

      console.log('\n-- Error auth.controller --')
      console.log(details);
      const errorServer = new Error('Error pada server.')
      res.status(401).send({
        status : '0',
        message : 'User Not Found',
        date: new Date(),
        details: err.message
      });
      next()
    });
};

exports.validedVoted = (req, res, next) => {
  new UserModel().getUserVoted(req.body)
    .then(user => {
      // if (!user) {
      //   return res.status(404).send({ message: "User Not found." });
      // }

      var token = jwt.sign({ appName: 'EVoting', userId: user.id }, config.secretApp);

      res.status(200).send({
        data : {
          status : '1',
          message : 'Login Sukses!',
          is_voted: user.is_voted,
          is_logined: user.is_logined,
          device_id: user.unique_device_id,
          payload : {
            id: user.id,
            nama: user.nama,
            email: user.email,
            nisn: user.nisn,
            npsn: user.npsn_sekolah_sekarang,
            sekolah: user.sekolah,
            phone_number: user.no_tlp
          }
        },
        accessToken: {
          tokenIs: token
        }
      });
    }).catch(err => {
      var details = {
        parent: err.parent,
        nameErr: err.name,
        messageErr: err.message
      }

      console.log('\n-- Error Valided.Voted auth.controller --')
      console.log(details);
      const errorServer = new Error('Error pada server.')
      res.status(401).send({
        data : {
          status: 0,
        },
        message : 'Unathorized',
        date: new Date(),
        details: err.message
      });
      next(errorServer)
    });
};

exports.postLogin = (req, res) => {
  new UserModel().postDataLogin(req.body)
    .then(x => {
      res.status(201).send({
          status: 1,
          message:"Sukses mengirim data"
      });
    }).catch( err => {
    var details = {
      parent:err.parent,
      name:err.name,
      message:err.message
    }

    console.log('---- Error.Post.Login auth.controller ----')
    console.log(details)
    res.status(409).send({
      status: 0,
      error: "Gagal Login!",
      statusCode: 409
    });
  });
};