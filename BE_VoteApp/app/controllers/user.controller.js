const UserModel = require("../models/user.model");
const dateHelper = require("../includes/dateHelper");
const { formatDate } = require("../includes/dateHelper");

exports.welcomeAll = (req, res) => {
  new UserModel().getDataWelcome()
    .then(x => {
      new UserModel().getDataCandidate()
      .then(y => {
        res.status(200).send({
          statusCode: 200,
          list_w: x,
          list_candidate: y
        });
      }).catch(error => {
        var details = {
          parent: error.parent,
          name: error.name,
          message: error.message
        }
        console.log('\n-- Error Welcome.user.controller --')
        console.log(details);
        res.status(203).send({
          data: {
            status: '0',
            statusCode: 203,
            message: 'Data Kosong!',
            date: new Date()
          }
        });
      })
    }).catch(err => {
      var details = {
        parent: err.parent,
        name: err.name,
        message: err.message
      }
      console.log('\n-- Error Welcome.user.controller --')
      console.log(details);
      res.status(203).send({
        data: {
          status: '0',
          statusCode: 203,
          message: 'Data Kosong!',
          date: new Date()
        }
      });
    }
  );
};

exports.voteCandidate = (req, res, next) => {
  new UserModel().postVoteCandidate(req.userId, req.body)
    .then(x => {
      new UserModel().getDataSync(req.userId)
        .then(user => {
          res.status(200).send({
            statusCode: 200,
            status: 1,
            message: 'Sukses Voting.',
            data : {
              status : 1,
              is_voted: user.is_voted,
              is_logined: user.is_logined,
              candidate_vote: user.candidate_vote,
              payload : {
                id: user.id,
                nama: user.nama,
                email: user.email,
                nisn: user.nisn,
                npsn: user.npsn_sekolah_sekarang,
                sekolah: user.sekolah,
                phone_number: user.no_tlp
              }
            },
            data_kandidat: {
              nama: user.nama_kandidat,
              image: user.image_kandidat
            }
          });
        }).catch(err => {
          var details = {
            parent: err.parent,
            name: err.name,
            message: err.message
          }
          console.log('\n-- Error getSync.user.controller --')
          console.log(details);
          res.status(203).send({
            data: {
              status: '0',
              statusCode: 203,
              message: 'Data Kosong!',
              date: new Date()
            }
          });
        }
      );
    }).catch( err => {
      var details = {
        parent:err.parent,
        name:err.name,
        message:err.message
      }

      console.log('---- Error.Post.Vote user.controller ----')
      console.log(details)
      res.status(409).send({
        status: 0,
        error: "Gagal Vote.",
        statusCode: 409
      });
      next();
    }
  );
};

exports.getSyncUser = (req, res) => {
  new UserModel().getDataSync(req.userId)
    .then(user => {
      res.status(200).send({
        statusCode: 200,
        data : {
          status : 1,
          is_voted: user.is_voted,
          is_logined: user.is_logined,
          candidate_vote: user.candidate_vote,
          payload : {
            id: user.id,
            nama: user.nama,
            email: user.email,
            nisn: user.nisn,
            npsn: user.npsn_sekolah_sekarang,
            sekolah: user.sekolah,
            phone_number: user.no_tlp
          }
        }
      });
    }).catch(err => {
      var details = {
        parent: err.parent,
        name: err.name,
        message: err.message
      }
      console.log('\n-- Error getSync.user.controller --')
      console.log(details);
      res.status(203).send({
        data: {
          status: '0',
          statusCode: 203,
          message: 'Data Kosong!',
          date: new Date()
        }
      });
    }
  );
};

exports.getQuickCount = (req, res) => {
  new UserModel().getDataCount()
    .then(x => {
      res.status(200).send({
        statusCode: 200,
        date_getCount: formatDate(new Date(), 'EE, d-MM-yyyy HH:mm'),
        labels: x.map(value => {
          return(value.first_name)
        }),
        data: x.map(value => {
          return(value.count_suara)
        }),
      });
    }).catch(err => {
      var details = {
        parent: err.parent,
        name: err.name,
        message: err.message
      }
      console.log('\n-- Error getCount.user.controller --')
      console.log(details);
      res.status(203).send({
        data: {
          status: '0',
          statusCode: 203,
          message: 'Data Kosong!',
          date: new Date()
        }
      });
    }
  );
};