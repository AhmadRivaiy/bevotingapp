const config = require("../config/db.config.js");
const { Op, Model, QueryTypes, DataTypes, Sequelize } = require('sequelize');
const dateHelper = require("../includes/dateHelper");
const { formatDate } = require("../includes/dateHelper");

const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: 0,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const User = sequelize.define("master_user", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  nama: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
  no_tlp: {
    type: Sequelize.INTEGER
  }
}, {
  freezeTableName: true,
  timestamps: false
});

const DummyUser = sequelize.define("dummy_user", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  unique_device_id: {
    type: Sequelize.CHAR
  }
}, {
  freezeTableName: true,
  timestamps: false
});

const WelcomeData = sequelize.define("welcome_screen", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  title: {
    type: Sequelize.STRING
  },
  deskripsi: {
    type: Sequelize.STRING
  },
  footer: {
    type: Sequelize.CHAR
  },
  image: {
    type: Sequelize.CHAR
  }
}, {
  freezeTableName: true
});

const CandidateData = sequelize.define("candidate", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  nama: {
    type: Sequelize.STRING
  },
  footer: {
    type: Sequelize.STRING
  },
  image: {
    type: Sequelize.CHAR
  }
}, {
  freezeTableName: true,
  timestamps: false
});
//JOIN TABLE
User.belongsTo(DummyUser, {targetkey: 'id', foreignKey: 'id'});
DummyUser.belongsTo(CandidateData, {targetkey: 'candidate_vote', foreignKey: 'candidate_vote'});

class UserModel {
  getUser = (username) => {
    return User.findOne({
      where: {
        no_tlp: username
        //[Op.or]: [{authorId: 12}, {authorId: 13}]
      },
      attributes: [
        'id',
        'nama',
        'email',
        'no_tlp',
        'nisn',
        'npsn_sekolah_sekarang',
        'dummy_user.unique_device_id',
        'dummy_user.candidate_vote',
        [sequelize.literal(`case when dummy_user.id is not null then 1 else 0 end`), 'is_logined'],
        [sequelize.literal(`case when dummy_user.is_vote is not null then 1 else 0 end`), 'is_voted'],
        [sequelize.literal(`case when dummy_user.unique_device_id is not null then 1 else 0 end`), 'define_device'],
        [sequelize.literal(`(select nama_sekolah from ref.tingkat_sma where npsn=npsn_sekolah_sekarang)`), 'sekolah']],
      include: [
          {
            model: DummyUser,
            required: false,
            attributes: ['id']
          }
      ],
      raw: true
    })
  }

  getUserVoted = (data) => {
    return User.findOne({
      where: {
        no_tlp: data.no_tlp
      },
      attributes: [
        'id',
        'nama',
        'email',
        'no_tlp',
        'nisn',
        'npsn_sekolah_sekarang',
        'dummy_user.unique_device_id',
        [sequelize.literal(`case when dummy_user.is_vote is not null then 1 else 0 end`), 'is_voted'],
        [sequelize.literal(`(select nama_sekolah from ref.tingkat_sma where npsn=npsn_sekolah_sekarang)`), 'sekolah']],
      include: [
          {
            model: DummyUser,
            where: {
              unique_device_id: data.id
            },
            required: true,
            attributes: ['id']
          }
      ],
      raw: true
    })
  }

  getDataWelcome = () => {
    return WelcomeData.findAll({
        where: {
          active: '1'
          //[Op.or]: [{authorId: 12}, {authorId: 13}]
        },
        order: [
          ['queue', 'ASC'],
        ],
        raw: true
      })
  }

  getDataCandidate = () => {
    return CandidateData.findAll({
        order: [
          ['id', 'ASC'],
        ],
        raw: true
      })
  }

  getDataSync = (userId) => {
    return User.findOne({
      where: {
        id: userId
        //[Op.or]: [{authorId: 12}, {authorId: 13}]
      },
      attributes: [
        'id',
        'nama',
        'email',
        'no_tlp',
        'nisn',
        'npsn_sekolah_sekarang',
        'dummy_user.candidate_vote',
        [sequelize.literal(`"dummy_user->candidate"."nama"`), 'nama_kandidat'],
        [sequelize.literal(`"dummy_user->candidate"."image"`), 'image_kandidat'],
        [sequelize.literal(`case when dummy_user.is_vote is not null then 1 else 0 end`), 'is_voted'],
        [sequelize.literal(`(select nama_sekolah from ref.tingkat_sma where npsn=npsn_sekolah_sekarang)`), 'sekolah']],
      include: [
          {
            model: DummyUser,
            required: false,
            attributes: ['id'],
            include: [
              {
                model: CandidateData,
                required: false,
                attributes: [],
              }
            ]
          },
      ],
      raw: true
    })
  }

  getDataCount = () => {
    return sequelize.query("SELECT SPLIT_PART(candidate.nama, ' ', 1) as first_name, candidate.nama AS nama_kandidat, dummy_user.candidate_vote, COUNT(dummy_user.candidate_vote) AS count_suara FROM public.dummy_user JOIN candidate ON dummy_user.candidate_vote = candidate.id GROUP BY candidate.nama, dummy_user.candidate_vote ORDER BY count_suara DESC", {
       raw:false,
       type:QueryTypes.SELECT 
    })
  }

  //POST ============================================================ = = = = = = = =
  postDataLogin = (data) => {
    return sequelize.query("insert into dummy_user(id, unique_device_id, created_at) values ('" + data.id + "','" + data.unique_id + "','" + formatDate(new Date(), 'yyyy-MM-d HH:mm:ss') + "')", {
        raw:true,
        type:QueryTypes.INSERT
    })
  }

  postVoteCandidate = (userId, data) => {
    return sequelize.query("UPDATE dummy_user SET is_vote='1', candidate_vote='" + data.idCandidate + "', update_at='" + formatDate(new Date(), 'yyyy-MM-d HH:mm:ss') + "' WHERE id='" + userId + "'", {
      raw:true,
      type:QueryTypes.UPDATE
    })
  }
}
module.exports = UserModel;